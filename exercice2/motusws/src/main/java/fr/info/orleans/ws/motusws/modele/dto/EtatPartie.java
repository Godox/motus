package fr.info.orleans.ws.motusws.modele.dto;

public class EtatPartie {

    private String mot;
    private String verdict;
    private int nbTentativesRestantes;

    public EtatPartie() {
        // NOP
    }

    public EtatPartie(String verdict, int nbTentativesRestantes) {
        this.verdict = verdict;
        this.nbTentativesRestantes = nbTentativesRestantes;
        this.mot = null;
    }

    public EtatPartie(String verdict, int nbTentativesRestantes, String mot) {
        this.verdict = verdict;
        this.nbTentativesRestantes = nbTentativesRestantes;
        this.mot = mot;
    }

    public String getVerdict() {
        return verdict;
    }

    public void setVerdict(String verdict) {
        this.verdict = verdict;
    }

    public int getNbTentativesRestantes() {
        return nbTentativesRestantes;
    }

    public void setNbTentativesRestantes(int nbTentativesRestantes) {
        this.nbTentativesRestantes = nbTentativesRestantes;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }
}
