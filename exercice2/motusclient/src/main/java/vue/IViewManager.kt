package vue

import controller.Controller
import controller.MainController
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import java.lang.reflect.ParameterizedType

interface IViewManager {

    val views: MutableList<IView<*>>
    val stage: Stage
    val mainController: MainController

    fun start()

    fun changeView(view: Class<out IView<*>>) {
        val parent = views.firstOrNull { it::class.java == view } as Parent
        stage.scene = parent.runCatching { Scene(this) }.getOrElse { parent.scene }
        stage.show()
    }

    fun loadViews(vararg viewsClasses: Class<out IView<Controller>>) {
        views.addAll(viewsClasses.map { clazz ->
            val instance = clazz.getConstructor().newInstance()
            instance.run {
                FXMLLoader(this::class.java.getResource("/fxml/$fxml")).let { fxml ->
                    fxml.setController(instance)
                    fxml.setRoot(instance)
                    fxml.load<Parent>()
                    val ctrl = mainController.controllers.first {
                        it::class.java == (instance.javaClass.genericInterfaces[0] as ParameterizedType).actualTypeArguments[0]
                    }
                    instance.controller = ctrl
                    (fxml.getController() as IView<*>).apply { initialize() }
                }
            }
        })
    }

}
