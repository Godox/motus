package vue

import controller.MainController
import controller.event.PopupEvent
import controller.event.ViewChangeEvent
import controller.event.manager.EventListener
import controller.event.manager.MotusEventHandler
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.control.TextField
import javafx.scene.image.Image
import javafx.scene.layout.VBox
import javafx.stage.Stage
import vue.fxml.Menu
import vue.fxml.Plateau

class ViewManager(override val stage: Stage) : IViewManager, EventListener {

    override val views: MutableList<IView<*>> = mutableListOf()
    override val mainController: MainController
        get() = MainController(this)

    init {
        subscribe()
        loadViews(Menu::class.java, Plateau::class.java)
    }

    override fun start() {
        stage.title = TITLE
        stage.icons += Image(this::class.java.getResourceAsStream(ICON_PATH))
        changeView(Menu::class.java)
    }

    @MotusEventHandler
    fun onChangeView(event: ViewChangeEvent) {
        changeView(event.view)
    }

    @MotusEventHandler
    fun onPopup(event: PopupEvent) {
        Alert(Alert.AlertType.INFORMATION, event.message).apply {
            title = event.title
            headerText = event.title
        }.showAndWait()
    }

    fun prompt(title: String, prompt: String): String {
        return Dialog<String>().apply {
            this.title = title
            dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
            val pseudoField = TextField()
            dialogPane.content = VBox(pseudoField.apply {
                promptText = prompt
            }).apply { alignment = Pos.CENTER }
            setResultConverter { if (it == ButtonType.OK) pseudoField.text else null }
            showAndWait()
        }.result
    }

    companion object {
        private const val ICON_PATH = "/img/icon.png"
        private const val TITLE = "Mo mo motus"
    }

}
