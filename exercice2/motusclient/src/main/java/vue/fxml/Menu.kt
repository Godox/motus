package vue.fxml

import controller.MenuController
import controller.event.UserRegisteredEvent
import controller.event.manager.MotusEventHandler
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.layout.VBox
import vue.IView

class Menu : VBox(), IView<MenuController> {

    override val fxml = "menu.fxml"
    override lateinit var controller: MenuController

    @FXML
    lateinit var pseudoField: TextField

    @FXML
    lateinit var validPseudoButton: Button

    @FXML
    lateinit var createGameButton: Button

    init {
        subscribe()
    }

    override fun initialize() {
        pseudoField.onKeyPressed = EventHandler { if (it.code == KeyCode.ENTER) onPseudoValid() }
    }

    @FXML
    fun onPseudoValid() {
        controller.creerUnCompte(pseudoField.text)
    }

    fun onGameCreated() {
        controller.creerUnePartie()
    }

    @MotusEventHandler
    fun onUserRegistered(event: UserRegisteredEvent) {
        pseudoField.isDisable = true
        validPseudoButton.isDisable = true
        createGameButton.isDisable = false
    }

}