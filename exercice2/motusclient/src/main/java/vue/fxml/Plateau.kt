package vue.fxml

import controller.GameController
import controller.event.*
import controller.event.manager.EventManager
import controller.event.manager.MotusEventHandler
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import javafx.scene.shape.Shape
import javafx.scene.text.Font
import vue.IView
import kotlin.concurrent.thread

class Plateau : IView<GameController>, BorderPane() {

    override val fxml = "Motus.fxml"
    override lateinit var controller: GameController

    @FXML
    lateinit var propositionField: TextField

    @FXML
    lateinit var answersContainer: VBox

    init {
        subscribe()
    }

    override fun initialize() {
        propositionField.onKeyPressed = EventHandler { if (it.code == KeyCode.ENTER) onValidate() }
    }

    private fun displayMot(reponse: String, mot: String, callback: () -> Unit = {}) {
        val hBox = HBox().apply {
            alignment = Pos.CENTER
            spacing = 5.0
        }
        answersContainer.children.add(hBox)
        thread {
            for ((index, letter) in reponse.withIndex()) {
                val square = when (letter) {
                    'X' -> getRightSquare()
                    'm' -> getWrongPlaceSquare()
                    '*' -> getWrongLetterSquare()
                    else -> throw NullPointerException()
                }
                Platform.runLater {
                    hBox.children.add(
                        StackPane(square, Label((mot.getOrElse(index) { 'X' }).toString().uppercase()).apply {
                            font = Font(30.0)
                        }))
                    playSoundForLetter(letter)
                }
                Thread.sleep(200)
            }
            callback()
        }
    }

    @MotusEventHandler
    fun onGameEnded(event: GameEndedEvent) {
        displayMot(event.verdict, event.proposition) {
            EventManager.fireEvent(PlaySoundEvent((if (event.win) "win" else "lose") + ".wav"))
            val messageVerdict = if (event.win) "gagné" else "perdu"
            Platform.runLater {
                EventManager.fireEvent(PopupEvent("Vous avez $messageVerdict ! Le mot était ${event.mot}",
                    messageVerdict.uppercase()))
                answersContainer.children.clear()
                propositionField.clear()
                EventManager.fireEvent(ViewChangeEvent(Menu::class.java))
            }
        }
    }

    @MotusEventHandler
    fun onPropositionAnswerReceived(event: PropositionAnswerEvent) {
        displayMot(event.propositionResult, event.mot)
    }

    private fun playSoundForLetter(letter: Char) {
        val sound = when (letter) {
            'X' -> "right.wav"
            'm' -> "exist.wav"
            '*' -> "wrong.wav"
            else -> throw NullPointerException()
        }
        EventManager.fireEvent(PlaySoundEvent(sound))
    }

    @FXML
    fun onValidate() {
        controller.proposerMot(propositionField.text)
        propositionField.clear()
    }

    //    un X pour une lettre à la bonne place
    fun getRightSquare(): Shape = Rectangle(50.0, 50.0, Color.RED)

    //    un m pour une lettre mal placée
    fun getWrongPlaceSquare(): Shape = Circle(25.0, Color.YELLOW)

    //    un * pour les mauvaises lettres
    fun getWrongLetterSquare(): Shape = Rectangle(50.0, 50.0, Color.LIGHTSKYBLUE)

}