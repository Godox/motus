package vue

import controller.Controller
import controller.event.manager.EventListener

interface IView<out T : Controller> : EventListener{

    var controller : @UnsafeVariance T
    val fxml: String

    fun initialize()

}