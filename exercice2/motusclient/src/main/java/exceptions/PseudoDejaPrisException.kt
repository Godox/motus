package exceptions

import java.lang.Exception

class PseudoDejaPrisException : Exception("Ce pseudo est déjà pris !")