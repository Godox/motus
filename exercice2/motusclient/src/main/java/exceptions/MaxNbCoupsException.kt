package exceptions

import java.lang.Exception

class MaxNbCoupsException : Exception("Max de coups déjà joués")