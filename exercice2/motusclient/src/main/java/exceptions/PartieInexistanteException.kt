package exceptions

import java.lang.Exception

class PartieInexistanteException : Exception("La partie n'existe pas !")