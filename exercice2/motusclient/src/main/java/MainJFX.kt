import controller.MainController
import vue.ViewManager
import javafx.application.Application
import javafx.stage.Stage
import kotlin.jvm.JvmStatic

class MainJFX : Application() {


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(MainJFX::class.java)
        }
    }


    override fun start(stage: Stage) {
        ViewManager(stage).start()
    }

}