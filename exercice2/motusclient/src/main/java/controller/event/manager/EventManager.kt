package controller.event.manager

import java.lang.reflect.Method

object EventManager {

    private val listeners: MutableMap<String, MutableMap<EventListener, MutableList<Method>>> = mutableMapOf()

    fun registerListener(eventListener: EventListener) {
        for (method in eventListener.javaClass.declaredMethods) {
            if (method.isAnnotationPresent(MotusEventHandler::class.java)) {
                try {
                    val parameter = method.parameters[0].type
                    if (MotusEvent::class.java.isAssignableFrom(parameter)) {
                        val eventName = parameter.name
                        if (!listeners.containsKey(eventName)) {
                            listeners[eventName] = HashMap()
                        }
                        val map = listeners[eventName]!!
                        if (!map.containsKey(eventListener)) {
                            map[eventListener] = ArrayList()
                        }
                        map[eventListener]!!.add(method)
                    } else {
                        error("Event method " + method.name + " first parameter is not an Event")
                    }
                } catch (e: IndexOutOfBoundsException) {
                    error("Event method " + method.name + " first parameter not exist")
                }
            }
        }
    }

    fun fireEvent(event: MotusEvent) {
        val eventName: String = event::class.java.name
        val eventListeners: Map<EventListener, List<Method>>? = listeners[eventName]
        eventListeners?.forEach { (eventListener: EventListener?, methods: List<Method>) ->
            methods.forEach {
                it.runCatching {
                    invoke(eventListener, event)
                }.onFailure { e -> error(e.cause!!) }
            }
        }
    }


}