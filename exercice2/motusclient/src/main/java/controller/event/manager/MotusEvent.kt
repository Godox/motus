package controller.event.manager

import java.io.Serializable

interface MotusEvent : Serializable {
    val eventName: String
        get() = this::class.java.simpleName
}