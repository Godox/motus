package controller.event.manager

interface EventListener {
    fun subscribe() {
        EventManager.registerListener(this)
    }
}