package controller.event

import controller.event.manager.MotusEvent

class GameEndedEvent(val win: Boolean, val verdict: String, val proposition: String, val mot: String) : MotusEvent
