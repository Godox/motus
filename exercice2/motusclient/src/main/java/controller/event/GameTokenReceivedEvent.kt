package controller.event

import controller.event.manager.MotusEvent

class GameTokenReceivedEvent(val tokenGame: String) : MotusEvent
