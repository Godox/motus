package controller.event

import controller.event.manager.MotusEvent

class PlaySoundEvent(val soundName: String) : MotusEvent