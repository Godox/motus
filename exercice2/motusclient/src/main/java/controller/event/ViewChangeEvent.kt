package controller.event

import controller.event.manager.MotusEvent
import vue.IView

class ViewChangeEvent(val view: Class<out IView<*>>) : MotusEvent