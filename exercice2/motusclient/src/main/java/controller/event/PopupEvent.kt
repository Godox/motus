package controller.event

import controller.event.manager.MotusEvent

class PopupEvent(val message: String?, val title: String = "Message") : MotusEvent

