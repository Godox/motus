package controller.event

import controller.event.manager.MotusEvent

class PropositionAnswerEvent(val propositionResult: String, val nbTentativesRestantes: Int, val mot: String) : MotusEvent
