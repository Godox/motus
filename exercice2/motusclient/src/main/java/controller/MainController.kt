package controller

import modele.MotusProxyImpl
import vue.IViewManager

class MainController(val viewManager: IViewManager) {

    val facade = MotusProxyImpl()

    val controllers: List<Controller> = listOf(
        GameController(this, facade, viewManager),
        MenuController(this, facade, viewManager)
    )

    init {
        SoundController()
    }

}
