package controller

import vue.IViewManager

abstract class Controller(val mainCtrl: MainController, val viewManager: IViewManager)
