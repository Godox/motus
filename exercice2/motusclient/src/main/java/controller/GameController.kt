package controller

import controller.event.GameEndedEvent
import controller.event.GameTokenReceivedEvent
import controller.event.PopupEvent
import controller.event.PropositionAnswerEvent
import controller.event.manager.EventListener
import controller.event.manager.EventManager
import controller.event.manager.MotusEventHandler
import exceptions.MotInexistantException
import modele.MotusProxy
import vue.IViewManager

class GameController(val mainController: MainController, val facade: MotusProxy, viewManager: IViewManager) :
    Controller(mainController, viewManager), EventListener {

    lateinit var gameToken: String

    init {
        subscribe()
    }

    @MotusEventHandler
    fun recevoirToken(event: GameTokenReceivedEvent) {
        gameToken = event.tokenGame
    }

    fun proposerMot(proposition: String) {
        if (proposition.length == 7) {
            try {
                val result = facade.proposerMot(gameToken, proposition)
                when {
                    result.verdict.all { it == 'X' } -> EventManager.fireEvent(GameEndedEvent(true,
                        result.verdict,
                        proposition, result.mot))
                    result.nbTentativesRestantes == 0 -> EventManager.fireEvent(GameEndedEvent(result.verdict.all { it == 'X' },
                        result.verdict,
                        proposition, result.mot))
                    else -> EventManager.fireEvent(PropositionAnswerEvent(result.verdict,
                        result.nbTentativesRestantes,
                        proposition))
                }
            } catch (except: MotInexistantException) {
                EventManager.fireEvent(PopupEvent("Ce mot n'existe pas !", "Erreur !"))
            }
        } else {
            EventManager.fireEvent(PopupEvent("Votre mot doit faire 7 lettres !", "Erreur !"))
        }

    }

}
