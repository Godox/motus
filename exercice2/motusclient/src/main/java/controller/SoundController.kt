package controller

import controller.event.PlaySoundEvent
import controller.event.manager.EventListener
import controller.event.manager.MotusEventHandler
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import java.io.File
import java.io.InputStream
import java.net.URI


class SoundController : EventListener {

    init {
        subscribe()
    }

    @MotusEventHandler
    fun onSoundEventReceived(event: PlaySoundEvent) {
        MediaPlayer(Media(File(getSoundPath(event.soundName)).toURI().toString())).play()
    }

    private fun getSoundPath(soundName: String): URI {
        return javaClass.getResource("/sound/$soundName").toURI()
    }
}
