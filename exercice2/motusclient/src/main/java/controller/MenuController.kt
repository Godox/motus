package controller

import controller.event.GameTokenReceivedEvent
import controller.event.PopupEvent
import controller.event.UserRegisteredEvent
import controller.event.ViewChangeEvent
import controller.event.manager.EventManager
import modele.MotusProxy
import vue.IViewManager
import vue.fxml.Plateau

class MenuController(val mainController: MainController, val facade: MotusProxy, viewManager: IViewManager) : Controller(mainController, viewManager) {

    lateinit var playerToken: String

    fun creerUnCompte(pseudo: String) {
        if (pseudo.isNotBlank()) {
            runCatching {
                playerToken = facade.creerUnCompte(pseudo)
                EventManager.fireEvent(UserRegisteredEvent())
                EventManager.fireEvent(PopupEvent("Bienvenu $pseudo !", "Bienvenu"))
            }.onFailure { EventManager.fireEvent(PopupEvent(it.message, "Erreur !")) }
        } else {
            EventManager.fireEvent(PopupEvent("Impossible d'avoir un pseudo vide !", "Erreur !"))
        }
    }

    fun creerUnePartie() {
        if (this::playerToken.isInitialized) {
            val tokenGame = facade.creerUnePartie(playerToken)
            EventManager.fireEvent(GameTokenReceivedEvent(tokenGame))
            EventManager.fireEvent(ViewChangeEvent(Plateau::class.java))
        }
    }

}