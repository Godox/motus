package modele

import com.google.gson.Gson
import exceptions.*
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class MotusProxyImpl : MotusProxy {
    private val httpClient = HttpClient.newHttpClient()
    private val serverUrl = "http://localhost:8080"
    private val gson = Gson()

    private fun sendRequest(
        endpoint: String,
        verb: String = "GET",
        urlParam: Map<String, String> = emptyMap(),
        headers: Map<String, String> = emptyMap()
    ): HttpResponse<String> {

        val url = URI(serverUrl + endpoint +
                //Build URL params
                urlParam.map { Pair(it.key, it.value) }.joinToString("&", "?") { "${it.first}=${it.second}" })

        return httpClient.send(HttpRequest.newBuilder(url).method(verb, HttpRequest.BodyPublishers.noBody()).apply {
            //Add Headers
            headers.forEach { (k, v) -> header(k, v) }
        }.build()) { HttpResponse.BodySubscribers.ofString(Charsets.UTF_8) }

    }

    @Throws(PseudoDejaPrisException::class)
    override fun creerUnCompte(pseudo: String): String {
        return sendRequest("/motus/joueur", "POST", urlParam = mapOf("pseudo" to pseudo)).run {
            if (statusCode() == 409) throw PseudoDejaPrisException()
            headers().map()["token"]?.get(0)!!
        }
    }

    override fun creerUnePartie(tokenAuthentification: String): String {
        return sendRequest("/motus/partie", "POST", headers = mapOf("token" to tokenAuthentification)).run {
            headers().map()["tokenpartie"]?.get(0)!!
        }
    }

    @Throws(MotInexistantException::class, MaxNbCoupsException::class, TicketInvalideException::class)
    override fun proposerMot(tokenPartie: String, proposition: String): EtatPartie {
        return sendRequest(
            "/motus/partie",
            "PUT",
            urlParam = mapOf("proposition" to proposition),
            headers = mapOf("tokenPartie" to tokenPartie)
        ).run {
            when (statusCode()) {
                404 -> throw MotInexistantException()
                406 -> throw MaxNbCoupsException()
                400 -> throw TicketInvalideException()
            }
            gson.fromJson(body(), EtatPartie::class.java)
        }
    }

    @Throws(TicketInvalideException::class, PartieInexistanteException::class)
    override fun getPropositions(tokenPartie: String): List<String> {
        return sendRequest("/motus/partie", headers = mapOf("tokenPartie" to tokenPartie)).run {
            when (statusCode()) {
                404 -> throw PartieInexistanteException()
                400 -> throw TicketInvalideException()
            }
            gson.fromJson<List<String>>(body(), List::class.java)
        }
    }
}