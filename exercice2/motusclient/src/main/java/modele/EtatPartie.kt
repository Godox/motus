package modele

class EtatPartie @JvmOverloads constructor(var verdict: String, var nbTentativesRestantes: Int = 0, val mot: String) {

    override fun toString(): String {
        return "EtatPartie: verdict : \"$verdict\", tentatives restantes : $nbTentativesRestantes"
    }

}