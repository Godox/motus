package modele

import exceptions.*
import kotlin.Throws

interface MotusProxy {
    @Throws(PseudoDejaPrisException::class)
    fun creerUnCompte(pseudo: String): String

    fun creerUnePartie(tokenAuthentification: String): String

    @Throws(MotInexistantException::class, MaxNbCoupsException::class, TicketInvalideException::class)
    fun proposerMot(tokenPartie: String, proposition: String): EtatPartie

    @Throws(TicketInvalideException::class, PartieInexistanteException::class)
    fun getPropositions(tokenPartie: String): List<String>
}