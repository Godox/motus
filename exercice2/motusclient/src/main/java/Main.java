import exceptions.*;
import modele.MotusProxy;
import modele.MotusProxyImpl;
import modele.EtatPartie;

import java.util.List;

public class Main {


    public static void main(String[] args) throws PseudoDejaPrisException, TicketInvalideException, PartieInexistanteException, MaxNbCoupsException, MotInexistantException {
        MotusProxy motusProxy = new MotusProxyImpl();
        String tokenAuthentification = motusProxy.creerUnCompte("Yohan2");
        String tokenPartie = motusProxy.creerUnePartie(tokenAuthentification);
        List<String> tentatives = motusProxy.getPropositions(tokenPartie);
        System.out.println(tentatives);
        EtatPartie etat = motusProxy.proposerMot(tokenPartie,"acheter");
        tentatives = motusProxy.getPropositions(tokenPartie);
        System.out.println(etat);
        System.out.println(tentatives);
    }

}
